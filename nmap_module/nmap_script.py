import nmap
from loguru import logger


def nmap_host_discovery(network_to_scan):
    logger.info("Début du scan NMAP...")
    clients = []
    nmap_scan = nmap.PortScanner()
    nmap_scan.scan(hosts=network_to_scan, arguments='-sn --min-parallelism=100')
    # print(nmap_scan.command_line())

    for host in nmap_scan.all_hosts():
        clients.append({'ip': host, 'mac': None})

    return clients


def nmap_port_scan_tcp(clients):
    logger.info("Scan des ports de {nb_clients} machines...", nb_clients=len(clients))
    nmap_scan = nmap.PortScanner()
    for c in clients:
        nmap_scan.scan(hosts=c['ip'], arguments='-sT -F')
        c['open_ports'] = []

        # If statement to avoid error with self-scan (all ports closed when self-scan)
        if 'tcp' in nmap_scan[c['ip']]:
            ports = nmap_scan[c['ip']]['tcp'].keys()

            for port in ports:
                # Check if port is open
                if nmap_scan[c['ip']]['tcp'][port]['state'] == "open":
                    c['open_ports'].append(port)

    return clients

