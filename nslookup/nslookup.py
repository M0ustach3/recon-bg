from scapy.all import Ether, ARP, sr1, conf, IP, UDP, RandShort, DNS, DNSQR


def nslookup(ip_to_query, dns_server_to_contact):
    conf.verb = 0
    query_name = ".".join(reversed(ip_to_query.split("."))) + ".in-addr.arpa"
    ans = sr1(IP(dst=dns_server_to_contact) / UDP(sport=RandShort(), dport=53) / DNS(rd=1,
                                                                                     qd=DNSQR(qname=query_name,
                                                                                              qtype="PTR")))
    if ans.an is not None:
        return ans.an.rdata.decode("utf-8")[:-1]
    else:
        return None
