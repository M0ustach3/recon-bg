from scapy.all import Ether, ARP, srp, conf
from loguru import logger


def arp_scanning(network_to_scan):
    logger.info("Début du scan ARP...")
    conf.verb = 0
    packet = Ether(dst="ff:ff:ff:ff:ff:ff") / ARP(pdst=network_to_scan)
    result = srp(packet, timeout=3)[0]
    clients = []

    for sent, received in result:
        clients.append({'ip': received.psrc, 'mac': received.hwsrc})

    return clients
