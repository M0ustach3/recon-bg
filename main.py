import json
import pprint
import csv
import sys

from arp.arp import arp_scanning
from mdns.mdns import mdns_host_discovery
from nmap_module.nmap_script import nmap_host_discovery, nmap_port_scan_tcp
from nslookup import nslookup
import argparse
from loguru import logger


def scan_network(program_arguments):
    arp_data = []
    nmap_data = []
    mdns_data = []
    if program_arguments.arp:
        arp_data = arp_scanning(program_arguments.target)
    if program_arguments.nmap:
        nmap_data = nmap_host_discovery(program_arguments.target)
    if program_arguments.mdns:
        mdns_data = mdns_host_discovery()

    for item in nmap_data:
        search = next((temp for temp in arp_data if temp['ip'] == item['ip']), None)
        if search is None:
            arp_data.append(item)

    for item in mdns_data:
        for arp in arp_data:
            arp['mdns_name'] = None
            arp['service_name'] = None
            if arp['ip'] == item['ip']:
                arp['mdns_name'] = item['mdns_name']
                arp['service_name'] = item['service_name']

    return arp_data


def port_scan_network(clients):
    data = nmap_port_scan_tcp(clients)
    return data


def dns_lookup_network(clients, dns_server):
    logger.info("Récupération des noms DNS de {nb_clients} machines auprès du serveur DNS {serveur_dns}",
                nb_clients=len(clients), serveur_dns=dns_server)
    for item in clients:
        dns_name = nslookup.nslookup(item['ip'], dns_server)
        item['dns_name'] = dns_name
    return clients


def pretty_print_hosts(clients):
    logger.info("Machines découvertes : ")
    for client in clients:
        logger.info("{}\t({})\t[{}]".format(client['ip'], client['mac'], ",".join([str(i) for i in client['open_ports']])))


def run_program(program_args):
    logger.info("Début de la découverte du réseau {network}", network=program_args.target)
    clients_result = scan_network(program_args)
    if not program_args.no_nmap_port_scan:
        clients_result = port_scan_network(clients_result)
    if not program_args.no_nslookup:
        clients_result = dns_lookup_network(clients_result, program_args.dns)

    logger.info("Fin de la découverte du réseau {network}", network=program_args.target)

    if program_args.csv:
        with open(program_args.csv, 'w') as file:
            writer = csv.writer(file)

            if not program_args.no_nmap_port_scan:
                total_ports = []
                for item in clients_result:
                    total_ports += item['open_ports']
                total_ports = list(set(total_ports))
                total_ports.sort()

            header = []

            # Construction du header
            if not program_args.no_nslookup:
                header.append('Nom DNS')
            header.append("IP")
            if program_args.arp:
                header.append("MAC")
            if program_args.mdns:
                header.append("Nom MDNS")
                header.append("Services exposés")
            if not program_args.no_nmap_port_scan:
                header.append("Total ports ouverts")
                header += total_ports
            writer.writerow(header)

            for item in clients_result:
                service_names = ""
                if 'service_name' in item:
                    for service in item['service_name'] or []:
                        if service is not None:
                            if service_names == "":
                                service_names = service
                            else:
                                service_names = service_names + "\n" + service

                rowdata = []
                if not program_args.no_nslookup:
                    rowdata.append(item['dns_name'])
                rowdata.append(item['ip'])
                if program_args.arp:
                    rowdata.append(item['mac'])
                if program_args.mdns:
                    rowdata.append(item['mdns_name'] if 'mdns_name' in item else "")
                    rowdata.append(service_names)
                if not program_args.no_nmap_port_scan:
                    rowdata.append(len(item['open_ports']))
                    for port in total_ports:
                        if port in item['open_ports']:
                            rowdata.append(1)
                        else:
                            rowdata.append("")
                writer.writerow(rowdata)
            logger.info("Le fichier CSV {fichier} à été écrit.", fichier=program_args.csv)
    if program_args.json:
        with open(program_args.json, 'w') as outfile:
            json.dump(json.dumps(clients_result), outfile)
            logger.info("Le fichier JSON {fichier} à été écrit.", fichier=program_args.json)

    pretty_print_hosts(clients_result)


if __name__ == "__main__":
    logger.remove()
    logger.add(sys.stdout, colorize=True, format="<green>{time:DD/MM/YYYY HH:mm:ss}</green> <level>{message}</level>")
    logger.info("Démarrage de Recon-bg")
    parser = argparse.ArgumentParser(description='Outil de cartographie active')
    ########
    # Paramètres de scan
    ########

    groupe_scan = parser.add_argument_group("Paramètres de scan",
                                            "Permet de régler les paramètres de scan des machines.")
    groupe_scan.add_argument('--target', type=str,
                             help="permet de définir l'ip ou la plage d'ips à découvrir", required=True)

    groupe_scan.add_argument('--dns', type=str,
                             help="permet de définir l'adresse IP du serveur DNS à contacter pour le reverse DNS",
                             required=True)

    groupe_scan.add_argument('--arp', action="store_true", help="effectue une découverte ARP du réseau")
    groupe_scan.add_argument('--mdns', action="store_true", help="effectue une découverte MDNS du réseau")

    groupe_scan.add_argument('--nmap', action="store_true", help="effectue une découverte NMAP du réseau")

    groupe_supplementaire = parser.add_argument_group("Paramètres d'informations supplémentaires",
                                                      "Permet de régler les paramètres de récupération d'informations complémentaires sur les machines trouvées grâce aux méthodes de scan.")
    groupe_supplementaire.add_argument('--no-nmap-port-scan', action="store_true",
                                       help="NE scanne PAS les ports des machines")
    groupe_supplementaire.add_argument('--no-nslookup', action="store_true",
                                       help="NE recherche PAS le nom DNS des machines")
    ########
    # Paramètres d'export
    ########

    groupe_export = parser.add_argument_group("Paramètres d'export",
                                              "Permet de régler les paramètres d'exportation des données. "
                                              "Par défaut, seulement un affichage dans la console sera présenté.")

    groupe_export.add_argument('--csv', type=str,
                               help="définit le fichier à utiliser pour exporter les données en CSV")
    groupe_export.add_argument('--json', type=str,
                               help="définit le fichier à utiliser pour exporter les données en JSON")
    args = parser.parse_args()

    run_program(args)
