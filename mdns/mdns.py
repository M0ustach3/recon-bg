import socket
from loguru import logger
from zeroconf import ZeroconfServiceTypes, ServiceBrowser, Zeroconf, ServiceListener
import time


def mdns_host_discovery():
    logger.info("Début du scan MDNS...")
    clients = []

    class MyListener(ServiceListener):

        def update_service(self, zc: Zeroconf, type_: str, name: str) -> None:
            return

        def remove_service(self, zc: Zeroconf, type_: str, name: str) -> None:
            return

        def add_service(self, zc: Zeroconf, type_: str, name: str) -> None:
            info = zc.get_service_info(type_, name)
            found = False

            # Check if host is already known
            for c in clients:
                if c['ip'] == socket.inet_ntoa(info.addresses[0]):
                    found = True
                    c['service_name'].append(info.name)

            if not found:
                clients.append(
                    {'ip': socket.inet_ntoa(info.addresses[0]), 'mdns_name': info.server, 'service_name': [info.name]})

    services_available = ZeroconfServiceTypes.find()

    for service in services_available:
        zeroconf = Zeroconf()
        listener = MyListener()
        browser = ServiceBrowser(zeroconf, service, listener)

        try:
            time.sleep(5)
        finally:
            zeroconf.close()

    return clients
