# Recon-bg

Projet de cartographie active pour le laboratoire CNS.

Version initiale de Pablo BONDIA-LUTTIAU et Théophile HAMELIN, promo 2022.

## Installation

- Etre sous GNU/Linux
- Installer Python 3 et PIP
- Installer Nmap via le package manager
- [OPTIONNEL] Créer un nouvel environnement virtualenv Python avec ```python3 -m venv ./venv-recon-bg``` et l'activer avec ```source venv-recon-bg/bin/activate```
- Installer les packages Python avec ```pip install -r requirements.txt```

## Aide du programme
```
usage: main.py [-h] --target TARGET --dns DNS [--arp] [--mdns] [--nmap] [--no-nmap-port-scan] [--no-nslookup] [--csv CSV] [--json JSON]

Outil de cartographie active

optional arguments:
  -h, --help           show this help message and exit

Paramètres de scan:
  Permet de régler les paramètres de scan des machines.

  --target TARGET      permet de définir l'ip ou la plage d'ips à découvrir
  --dns DNS            permet de définir l'adresse IP du serveur DNS à contacter pour le reverse DNS
  --arp                effectue une découverte ARP du réseau
  --mdns               effectue une découverte MDNS du réseau
  --nmap               effectue une découverte NMAP du réseau

Paramètres d'informations supplémentaires:
  Permet de régler les paramètres de récupération d'informations complémentaires sur les machines trouvées grâce aux méthodes de scan.

  --no-nmap-port-scan  NE scanne PAS les ports des machines
  --no-nslookup        NE recherche PAS le nom DNS des machines

Paramètres d'export:
  Permet de régler les paramètres d'exportation des données. Par défaut, seulement un affichage dans la console sera présenté.

  --csv CSV            définit le fichier à utiliser pour exporter les données en CSV
  --json JSON          définit le fichier à utiliser pour exporter les données en JSON
```

## Exemple d'utilisation

```python main.py --target 192.168.1.0/24 --dns 192.168.1.250 --arp --mdns --nmap --csv fichier_sortie.csv```
